const xml2js = require('xml2js');
const fs = require('fs');
const path = require('path');

const parser = new xml2js.Parser({ attrkey: "ATTR" });


exports.get_data_from_kml = () => {
    const file_path = path.join(__dirname, '..', 'assets', 'FullStackTest_DeliveryAreas.kml');
    let xml_string = fs.readFileSync(file_path, "utf8");
    const polygon_detail_array = []
    parser.parseString(xml_string, function (error, result) {
        if (error === null) {
            const placemark = result.kml.Document[0].Placemark;
            placemark.forEach(item => {
                let polygon_store_details = {};
                polygon_store_details.name = item.name[0]
                if (item.Point !== undefined) {
                    let result = [];
                    let points = item.Point[0].coordinates[0].trim().split('\n');
                    let coordinates_array = [points[0].split(',')[1],
                    points[0].split(',')[0]
                    ]
                    result.push(coordinates_array)
                    polygon_store_details.coordinates = result
                } else if (item.Polygon.length > 0) {
                    let result = []
                    item.Polygon[0].outerBoundaryIs[0].LinearRing[0].coordinates[0].trim().split('\n').map(ele => {
                        let coordinates_array = []
                        const longitude = ele.trim().split(',')[0]
                        const latitude = ele.trim().split(',')[1]
                        coordinates_array.push(latitude, longitude)
                        result.push(coordinates_array)
                    })
                    polygon_store_details.coordinates = result
                }
                polygon_detail_array.push(polygon_store_details)
            })
        } else {
            return error
        }
    })
    return polygon_detail_array
}


exports.get_storename = (point, polygon) => {
    let x = point[0], y = point[1];

    let inside = false;
    polygon.forEach(({ name, coordinates }) => {
        for (let i = 0, j = coordinates.length - 1; i < coordinates.length; j = i++) {
            let xi = coordinates[i][0]
            let yi = coordinates[i][1];
            let xj = coordinates[j][0]
            let yj = coordinates[j][1];

            let intersect = ((yi > y) != (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
            if (inside) {
                return name
            }else {
                return false
            }
        }
    })
}
