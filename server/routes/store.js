const express = require('express')

const storeController = require('../controllers/store');

const router = express.Router();

router.post('/get_nearby_store', storeController.getNearByStore);

module.exports = router