const utilityFunctions = require('../utility/data_from_kml');

exports.getNearByStore = async (req, res, next) => {
    try {

        const latitude = req.body.latitude;
        const longitude = req.body.longitude;
        const polygon_data = await utilityFunctions.get_data_from_kml();
        if (!polygon_data) {
            const error = new Error('Something went wrong')
            error.statusCode = 500
            error.data = errors.array()
            throw error;
        }
        // const result = await utilityFunctions.get_storename([latitude, longitude], polygon_data)
        const result = await utilityFunctions.get_storename([48.1914916, 16.4002954], polygon_data)
        if (!result) {
            res.status(200).json({
                message: 'No Store Found'
            })
        } else {
            res.status(200).json({
                message: 'Store Found',
                name: result
            })
        }

    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error)
    }
}