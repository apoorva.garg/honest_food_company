import React from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom'
import './App.css';

import MainNavigation from './components/Navigation/MainNavigation';
import SearchStore from './components/SearchStore/SearchStore';


const app = () => {
  return (
    <BrowserRouter>
      <React.Fragment>
        <MainNavigation />
        <main className="main-content">
          <Switch>
            <Redirect from="/" to="/searchstore" exact />
            <Route path="/searchstore" component={SearchStore} />
          </Switch>
        </main>
      </React.Fragment>
    </BrowserRouter >
  );
}

export default app;