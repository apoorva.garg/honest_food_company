import React from 'react';
import { NavLink } from 'react-router-dom';

import './MainNavigation.css';

const mainNavigation = (props) => {
    return (
        <header className="main-navigation">
            <div className="main-navigation-title">
                <h1>Geo Location</h1>
            </div>
            <nav className="main-navigation-items">
                <ul>
                    <li><NavLink to="/searchstore">Search Store</NavLink></li>
                </ul>
            </nav>
        </header>
    )
}

export default mainNavigation