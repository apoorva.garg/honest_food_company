import React, { useRef, useState } from 'react'

import Geocode from "react-geocode";

import './SearchStore.css'

import Input from '../Input/Input'
import Button from '../Button/Button'

const SearchStore = () => {
    const addressRef = useRef(null)
    const [store, setStore] = useState([])


    const getLocation = address => {
        Geocode.setApiKey("AIzaSyALrSTy6NpqdhIOUs3IQMfvjh71td2suzY");
        let location = {}
        Geocode.fromAddress(address).then(
            response => {
                console.log(response)
                location = response.results[0].geometry.location;
            },
            error => {
                console.error(error);
            }
        );
        return location
    }

    const searchStoreHandler = () => {
        const itemValue = addressRef.current.value;

        const { lat, long } = getLocation(itemValue)
        const requestBody = {
            latitude: lat,
            longitude: long
        }

        fetch(`http://localhost:8000/store/get_nearby_store`, {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            if (res.status !== 200 && res.status !== 201) {
                res.json().then(err => console.error(err.message))
                throw new Error(res)
            }
            return res.json()
        })
            .then((resData) => {
                setStore(resData)
            })
            .catch(err => {
                console.log(err)
            })
    }


    // const output = <p>No Stores found</p>
    // if (store.length > 0) {
    //     output = <p>Your Nearby Store name is {store[0].name}</p>
    // }

    return (
        <div className="main-container">
            <h3 className="search-header">Search Your Nearby Store</h3>
            <main>
                <Input label="Enter Address" type="text" placeholder="Enter Address" id="address" refel={addressRef}></Input>
                <div style={{ textAlign: 'center' }}>
                    <Button clicked={searchStoreHandler}>Search Store</Button>
                </div>
               
            </main>
        </div>
    );
}

export default SearchStore